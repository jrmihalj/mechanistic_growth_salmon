#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

# Data input and model run for STAN model
# Karl's fish growth data
# sh 2009-10 & 2012-13
# Authors: JR Mihaljevic, Ruby An

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

library(tidyverse)
options(dplyr.print_max = 1e9)
options(max.print = 2000)
library(xtable)
library(grid)
library(gridExtra)
library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

# Import and clean the data sets:
source("./code/Steelhead/DataClean_Steelhead.R")

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# MODEL 3:
# Compare ALL recaptures to all "others"

# Select the data:
# shdata_CRvTR
glimpse(shdata_CRvTR)
# Order so that Treatment comes first:
shdata_CRvTR =
  shdata_CRvTR %>%
  arrange(Habitat, FishID)

# PROBLEM: No recaptures from controls in 2013
## Need to remove 2013 data

shdata_CRvTR_clean =
  filter(shdata_CRvTR, Year != 4)

## Fix FishID values for 2016
shdata_CRvTR_clean =
  shdata_CRvTR_clean %>%
  rowwise() %>%
  mutate(FishID = ifelse((Habitat == 1 & Year == 5), FishID-21, FishID)) %>%
  mutate(FishID = ifelse((Habitat == 2 & Year == 5), FishID-1, FishID)) %>%
  mutate(Year = ifelse(Year == 5, 4, Year))
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

# Set up model parameters:
N_habitat <- length(unique(shdata_CRvTR_clean$Habitat))                 # Number of habitat types
N_year <- max(shdata_CRvTR_clean$Year)                                  # Number of years of data
N_recap_T <- max((shdata_CRvTR_clean %>% filter(Habitat == 1))$FishID)  # Number of treated individuals
N_recap_C <- max((shdata_CRvTR_clean %>% filter(Habitat == 2))$FishID)  # Number of control individuals
Nobs_T <- nrow(shdata_CRvTR_clean %>% filter(Habitat == 1))             # Number of treated observations
Nobs_C <- nrow(shdata_CRvTR_clean %>% filter(Habitat == 2))             # Number of control observations
Nobs_total <- nrow(shdata_CRvTR_clean)                                  # Total number of observations

# Covariates
Year <- shdata_CRvTR_clean$Year
Date <- shdata_CRvTR_clean$Time
SL <- shdata_CRvTR_clean$SL
# Habitat:
# (1): Recaps, Treated; (2): Recaps, Control
Habitat <- shdata_CRvTR_clean$Habitat
FishID <- shdata_CRvTR_clean$FishID
FishID_C <- (shdata_CRvTR_clean %>% filter(Habitat == 2))$FishID

# Plot the data
p1 <- ggplot(shdata_CRvTR_clean, aes(x=Time, y=SL, color=factor(Year)))+
  geom_jitter(aes(shape=factor(Habitat)))+
  scale_color_brewer("Year", labels=c("2009", "2010", "2012", "2016"), palette = "Set1")+
  scale_shape_manual("Type", labels=c("Recaptured\n(Treated)", "Recaptured\n(Control)"), values=c(20, 0))+
  theme_classic()+
  theme(legend.key.size = unit(.3, "inches")) +
  scale_y_continuous(limits = c(15, 100), breaks=c(25,50,75,100))+
  labs(x="Time Point", y="Standard Length (mm)")

quartz(height=5, width=7)
p1

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------


# Stan requires the data to be compiled into a named list, as follows:
stan_data <- list(N_habitat = N_habitat,
                  N_year = N_year,
                  N_recap_T = N_recap_T,
                  N_recap_C = N_recap_C,
                  Nobs_T = Nobs_T,
                  Nobs_C = Nobs_C,
                  Nobs_total = Nobs_total,
                  Year = Year,
                  Date = Date,
                  SL = SL,
                  Habitat = Habitat,
                  FishID = FishID)
                  # FishID_T = FishID_T,
                  # FishID_C = FishID_C)

str(stan_data)

################################################################################
# START THE MODEL STATEMENT
################################################################################

# Tell Stan for which parameters you want to get posteriors:
params <- c("Yp_hab", "Yinf_hab", "alpha_hat_hab", "a_hab",
            "eta_Yp", "eta_Yinf", "eta_alpha_hat", "eta_a",
            "eta_fish_T", "eta_fish_C",
            #"eta_fish_Yp", "eta_fish_Yinf", "eta_fish_alpha_hat", "eta_fish_a",
            "sigma_Yp", "sigma_Yinf", "sigma_alpha_hat", "sigma_a", 
            "sigma_fish_T", "sigma_fish_C",
            #"sigma_fish_Yp", "sigma_fish_Yinf", "sigma_fish_alpha_hat", "sigma_fish_a", 
            "sigma_resid", "diff_Yp", "diff_Yinf", "diff_alpha_hat", "diff_a",
            "diff_Yp_y", "diff_Yinf_y", "diff_alpha_hat_y", "diff_a_y",
            "total_growth")

# First, test to make sure your code works...
test <- stan("./code/Steelhead/multiyearSteelhead_CRvTR.stan", 
             pars = params,
             data = stan_data,
             chains = 1,
             iter = 10,
             thin = 1)

# Now update it with more iterations and chains:
out <- stan(fit = test,
            pars = params,
            chains = 3,
            warmup = 2000, 
            iter = 7000, # (iteration - warmup)/thin = output size 
            thin = 5,
            init = 0)

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
## Summarize and validate the model

# plot the chains quickly:
traceplot(out, pars="lp__")
traceplot(out, pars="Yp_hab")
traceplot(out, pars="Yinf_hab")
traceplot(out, pars="alpha_hat_hab")
traceplot(out, pars="a_hab")

# rand effs
traceplot(out, pars="eta_Yp")
traceplot(out, pars="eta_Yinf")
traceplot(out, pars="eta_alpha_hat")
traceplot(out, pars="eta_a")

traceplot(out, pars="sigma_Yp")
traceplot(out, pars="sigma_Yinf")
traceplot(out, pars="sigma_alpha_hat")
traceplot(out, pars="sigma_a")

# traceplot(out, pars="sigma_fish_Yp")
# traceplot(out, pars="sigma_fish_Yinf")
# traceplot(out, pars="sigma_fish_alpha_hat")
# traceplot(out, pars="sigma_fish_a")
traceplot(out, pars="sigma_fish_C")
traceplot(out, pars="sigma_fish_T")

traceplot(out, pars="sigma_resid")

#differences between habitats:
traceplot(out, pars=c("diff_Yp", "diff_Yinf", "diff_alpha_hat", "diff_a"))
traceplot(out, pars=c("diff_Yp_y", "diff_Yinf_y", "diff_alpha_hat_y", "diff_a_y"))
traceplot(out, pars="total_growth")

# check convergence:
hist(summary(out)$summary[,"Rhat"])

# extract the posteriors to a named list
all_posts <- extract(out) 
str(all_posts)


#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
years <- c(2009, 2010, 2012, 2016)

# Year, Med_Recap, Low_Recap, High_Recap, Med_Other, Low_Other, High_Other, Med_Dif, Low_Dif, High_Dif
hpd_colnames = c("Year", "Med_Treat", "Low_Treat", "High_Treat", "Med_Contr", "Low_Contr", "High_Contr", "Med_Dif", "Low_Dif", "High_Dif")
hpd_alpha_hat = matrix(0, ncol = length(hpd_colnames), nrow=N_year+1)
hpd_a = matrix(0, ncol = length(hpd_colnames), nrow=N_year+1)
hpd_Yp = matrix(0, ncol = length(hpd_colnames), nrow=N_year+1)
hpd_Yinf = matrix(0, ncol = length(hpd_colnames), nrow=N_year+1)

# Inflection Point:
# $\hat{\alpha}/a$
hpd_inflect = matrix(0, ncol = length(hpd_colnames), nrow=N_year+1)
# Calculate difference in inflection point (other - recap):
inflect_recap = matrix(0, ncol = 5, nrow = 3000)
inflect_other = matrix(0, ncol = 5, nrow = 3000)
diff_inflect_y = matrix(0, ncol = 5, nrow = 3000)
# Overall inflection:
diff_inflect = NULL

for(i in 1:N_year){
  inflect_recap[,i] = (all_posts$alpha_hat_hab[,2] + all_posts$eta_alpha_hat[,2,i])/
    (all_posts$a_hab[,2] + all_posts$eta_a[,2,i])
  inflect_other[,i] = (all_posts$alpha_hat_hab[,1] + all_posts$eta_alpha_hat[,1,i])/
    (all_posts$a_hab[,1] + all_posts$eta_a[,1,i])
  
  diff_inflect_y[,i] = inflect_other[,i] - inflect_recap[, i]
}

diff_inflect = ((all_posts$alpha_hat_hab[,2])/(all_posts$a_hab[,2])) - 
  ((all_posts$alpha_hat_hab[,1])/(all_posts$a_hab[,1]))

# Intercept of the curve 
## (i.e., length at t = 0)
intercept_mat = matrix(0, ncol = N_year, nrow = 3000)


for(j in 1:N_year){
  
  temp_other = (all_posts$Yp_hab[,1] + all_posts$eta_Yp[,1,j]) +
    ((all_posts$Yinf_hab[,1] + all_posts$eta_Yinf[,1,j]) /
       ( 1 + exp( (all_posts$alpha_hat_hab[,1] + all_posts$eta_alpha_hat[,1,j]) ) ))
  
  temp_recap = (all_posts$Yp_hab[,2] + all_posts$eta_Yp[,2,j]) +
    ((all_posts$Yinf_hab[,2] + all_posts$eta_Yinf[,2,j]) /
       ( 1 + exp( (all_posts$alpha_hat_hab[,2] + all_posts$eta_alpha_hat[,2,j]) ) ))
  
  diff_intercept = temp_other - temp_recap
  
  intercept_mat[, j] = diff_intercept
  
}
# Combined (among-year) diffs 
temp_other_combo = all_posts$Yp_hab[,1] + (all_posts$Yinf_hab[,1] / ( 1 + exp(all_posts$alpha_hat_hab[,1]) ))
temp_recap_combo = all_posts$Yp_hab[,2] + (all_posts$Yinf_hab[,2] / ( 1 + exp(all_posts$alpha_hat_hab[,2]) ))
diff_intercept_combo = temp_other_combo - temp_recap_combo

hpd_intercept = matrix(0, ncol = length(hpd_colnames), nrow=N_year+1)


# STORE AND OUTPUT THE SUMMARIES

for(i in 1:(N_year+1)){
  
  if(i == (N_year + 1)){
    hpd_alpha_hat[i, 1] = NA
    hpd_alpha_hat[i, 2:4] = quantile((all_posts$alpha_hat_hab[,2]), probs = c(0.5, 0.025, 0.975))
    hpd_alpha_hat[i, 5:7] = quantile((all_posts$alpha_hat_hab[,1]), probs = c(0.5, 0.025, 0.975))
    hpd_alpha_hat[i, 8:10] = quantile(all_posts$diff_alpha_hat, probs = c(0.5, 0.025, 0.975))
    
    hpd_a[i, 1] = NA
    hpd_a[i, 2:4] = quantile((all_posts$a_hab[,2]), probs = c(0.5, 0.025, 0.975))
    hpd_a[i, 5:7] = quantile((all_posts$a_hab[,1]), probs = c(0.5, 0.025, 0.975))
    hpd_a[i, 8:10] = quantile(all_posts$diff_a, probs = c(0.5, 0.025, 0.975))
    
    hpd_inflect[i, 1] = NA
    hpd_inflect[i, 2:4] = quantile((all_posts$alpha_hat_hab[,2])/(all_posts$a_hab[,2]), 
                                   probs = c(0.5, 0.025, 0.975))
    hpd_inflect[i, 5:7] = quantile((all_posts$alpha_hat_hab[,1])/(all_posts$a_hab[,1]), 
                                   probs = c(0.5, 0.025, 0.975))
    hpd_inflect[i, 8:10] = quantile(diff_inflect, probs = c(0.5, 0.025, 0.975))
    
    hpd_Yp[i, 1] = NA
    hpd_Yp[i, 2:4] = quantile((all_posts$Yp_hab[,2]), probs = c(0.5, 0.025, 0.975))
    hpd_Yp[i, 5:7] = quantile((all_posts$Yp_hab[,1]), probs = c(0.5, 0.025, 0.975))
    hpd_Yp[i, 8:10] = quantile(all_posts$diff_Yp, probs = c(0.5, 0.025, 0.975))
    
    hpd_Yinf[i, 1] = NA
    hpd_Yinf[i, 2:4] = quantile((all_posts$Yinf_hab[,2]), probs = c(0.5, 0.025, 0.975))
    hpd_Yinf[i, 5:7] = quantile((all_posts$Yinf_hab[,1]), probs = c(0.5, 0.025, 0.975))
    hpd_Yinf[i, 8:10] = quantile(all_posts$diff_Yinf, probs = c(0.5, 0.025, 0.975))
    
    hpd_intercept[i, 1] = NA
    hpd_intercept[i, 2:4] = NA
    hpd_intercept[i, 5:7] = NA
    hpd_intercept[i, 8:10] = quantile(diff_intercept_combo, probs = c(0.5, 0.025, 0.975))
    
    
  }else{
    hpd_alpha_hat[i, 1] = years[i]
    hpd_alpha_hat[i, 2:4] = quantile((all_posts$alpha_hat_hab[,2] + all_posts$eta_alpha_hat[,2,i]), probs = c(0.5, 0.025, 0.975))
    hpd_alpha_hat[i, 5:7] = quantile((all_posts$alpha_hat_hab[,1] + all_posts$eta_alpha_hat[,1,i]), probs = c(0.5, 0.025, 0.975))
    hpd_alpha_hat[i, 8:10] = quantile(all_posts$diff_alpha_hat_y[,i], probs = c(0.5, 0.025, 0.975))
    
    hpd_a[i, 1] = years[i]
    hpd_a[i, 2:4] = quantile((all_posts$a_hab[,2] + all_posts$eta_a[,2,i]), probs = c(0.5, 0.025, 0.975))
    hpd_a[i, 5:7] = quantile((all_posts$a_hab[,1] + all_posts$eta_a[,1,i]), probs = c(0.5, 0.025, 0.975))
    hpd_a[i, 8:10] = quantile(all_posts$diff_a_y[,i], probs = c(0.5, 0.025, 0.975))
    
    hpd_inflect[i, 1] = years[i]
    hpd_inflect[i, 2:4] = quantile(inflect_recap[,i], 
                                   probs = c(0.5, 0.025, 0.975))
    hpd_inflect[i, 5:7] = quantile(inflect_other[,i], 
                                   probs = c(0.5, 0.025, 0.975))
    hpd_inflect[i, 8:10] = quantile(diff_inflect_y[,i], probs = c(0.5, 0.025, 0.975))
    
    hpd_Yp[i, 1] = years[i]
    hpd_Yp[i, 2:4] = quantile((all_posts$Yp_hab[,2] + all_posts$eta_Yp[,2,i]), probs = c(0.5, 0.025, 0.975))
    hpd_Yp[i, 5:7] = quantile((all_posts$Yp_hab[,1] + all_posts$eta_Yp[,1,i]), probs = c(0.5, 0.025, 0.975))
    hpd_Yp[i, 8:10] = quantile(all_posts$diff_Yp_y[,i], probs = c(0.5, 0.025, 0.975))
    
    hpd_Yinf[i, 1] = years[i]
    hpd_Yinf[i, 2:4] = quantile((all_posts$Yinf_hab[,2] + all_posts$eta_Yinf[,2,i]), probs = c(0.5, 0.025, 0.975))
    hpd_Yinf[i, 5:7] = quantile((all_posts$Yinf_hab[,1] + all_posts$eta_Yinf[,1,i]), probs = c(0.5, 0.025, 0.975))
    hpd_Yinf[i, 8:10] = quantile(all_posts$diff_Yinf_y[,i], probs = c(0.5, 0.025, 0.975))
    
    hpd_intercept[i, 1] = years[i]
    hpd_intercept[i, 2:4] = NA
    hpd_intercept[i, 5:7] = NA
    hpd_intercept[i, 8:10] = quantile(intercept_mat[, i], probs = c(0.5, 0.025, 0.975))
  }
  
}

# Combine and do some cleaning:
hpd_all = data.frame(rbind(hpd_alpha_hat, hpd_a, hpd_inflect, hpd_Yp, hpd_Yinf, hpd_intercept))
colnames(hpd_all) = hpd_colnames
hpd_all$Param = rep(c("alpha_hat", "a", "Inflection", "Yp", "Yinf", "Intercept"), each = (N_year + 1))
hpd_all = hpd_all[, c(ncol(hpd_all), 1:(ncol(hpd_all)-1))]
# Output a Latex-style table:
print(xtable(hpd_all, digits = 3), comment = F)

# Store a csv:
write.csv(hpd_all, file = "./figures/Steelhead/HPD_Params_Steelhead_TRvCR2.csv")

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------


# simple histos
quartz()
par(mfrow=c(2,2))

Yp_posts <- all_posts$Yp_hab
hist(Yp_posts[,1], col="red", breaks=50, main=expression(Y^minute), xlab="")#, xlim=c(45, 65))
hist(Yp_posts[,2], col="black", breaks=50, add=T)

Yinf_posts <- all_posts$Yinf_hab
hist(Yinf_posts[,1], col="red", breaks=50, main=expression(Y[infinity]), xlab="")#, xlim=c(15, 30))
hist(Yinf_posts[,2], col="black", breaks=50, add=T)

alpha_hat_posts <- all_posts$alpha_hat_hab
hist(alpha_hat_posts[,2], col="black", breaks=50, main=expression(hat(alpha)), xlab="")#, xlim=c(0, 8), ylim=c(0, 225))
hist(alpha_hat_posts[,1], col="red", breaks=50, add=T)

a_posts <- all_posts$a_hab
hist(a_posts[,2], col="black", breaks=50, main=expression(a), xlab="")#, xlim=c(0, 0.25), ylim=c(0, 300))
hist(a_posts[,1], col="red", breaks=50, add=T)

quartz.save("./figures/Steelhead/CRvTR/Param_posts.pdf", type = "pdf", dpi = 300)

#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

# High density interval for alpha_hat
library(coda)

{ # Differences across years for all parameters
  quartz()
  par(mfrow=c(2,2), omi=c(0,0,.5,0))
  
  hist(all_posts$diff_Yp, main=expression(Y^minute), xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_Yp), nc=1)), col="black", lwd=2, lty=2)
  
  hist(all_posts$diff_Yinf, main=expression(Y[infinity]), xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_Yinf), nc=1)), col="black", lwd=2, lty=2)
  
  hist(all_posts$diff_alpha_hat, main=expression(hat(alpha)), xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_alpha_hat), nc=1)), col="black", lwd=2, lty=2)
  
  hist(all_posts$diff_a, main=expression(a), xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_a), nc=1)), col="black", lwd=2, lty=2)
}
quartz.save("./figures/Steelhead/CRvTR/Diffs_AllYears.pdf", type = "pdf", dpi = 300)


# Differences by year by parameter
years <- c(2009, 2010, 2012, 2013, 2016)

quartz()
par(mfrow=c(3,2), omi=c(0,0,.5,0))
for(i in 1:N_year){
  hist(all_posts$diff_Yp_y[,i], main=years[i], xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_Yp_y[,i]), nc=1)), col="black", lwd=2, lty=2)
}
mtext(expression(Y^minute), side=3, outer=T, line=0)
quartz.save("./figures/Steelhead/CRvTR/Diffs_Yp_Yearly.pdf", type = "pdf", dpi = 300)

quartz()
par(mfrow=c(3,2), omi=c(0,0,.5,0))
for(i in 1:N_year){
  hist(all_posts$diff_Yinf_y[,i], main=years[i], xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_Yinf_y[,i]), nc=1)), col="black", lwd=2, lty=2)
}
mtext(expression(Y[infinity]), side=3, outer=T, line=0)
quartz.save("./figures/Steelhead/CRvTR/Diffs_Yinf_Yearly.pdf", type = "pdf", dpi = 300)

quartz()
par(mfrow=c(3,2), omi=c(0,0,.5,0))
for(i in 1:N_year){
  hist(all_posts$diff_alpha_hat_y[,i], main=years[i], xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_alpha_hat_y[,i]), nc=1)), col="black", lwd=2, lty=2)
}
mtext(expression(hat(alpha)), side=3, outer=T, line=0)
quartz.save("./figures/Steelhead/CRvTR/Diffs_alphahat_Yearly.pdf", type = "pdf", dpi = 300)

quartz()
par(mfrow=c(3,2), omi=c(0,0,.5,0))
for(i in 1:N_year){
  hist(all_posts$diff_a_y[,i], main=years[i], xlab="", col="gray")
  abline(v=HPDinterval(as.mcmc(matrix(all_posts$diff_a_y[,i]), nc=1)), col="black", lwd=2, lty=2)
}
mtext(expression(a), side=3, outer=T, line=0)
quartz.save("./figures/Steelhead/CRvTR/Diffs_a_Yearly.pdf", type = "pdf", dpi = 300)

detach("package:coda", unload=TRUE)
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

# Plot with Median Prediction Lines

fake_x <- seq(0,100,length.out = 200)
fake_y <- NULL

for(i in 1:N_habitat){
  for(j in 1:N_year){
    
    temp <- (median(all_posts$Yp_hab[,i] + all_posts$eta_Yp[,i,j])) +
      ((median(all_posts$Yinf_hab[,i] + all_posts$eta_Yinf[,i,j])) /
         ( 1 + exp( (median(all_posts$alpha_hat_hab[,i] + all_posts$eta_alpha_hat[,i,j]))  -
                      (median(all_posts$a_hab[,i] + all_posts$eta_a[,i,j])) * fake_x
         )))
    
    fake_y <- c(fake_y, temp)
    
  }
}

fake_hab <- rep(c(1:N_habitat), each=length(fake_x)*N_year)
Year <- rep(rep(c(1:N_year), each=length(fake_x)), N_habitat)
Year = factor(Year)
levels(Year) = c("2009", "2010", "2012", "2016")
df_fake <- data.frame(fake_x, fake_y, fake_hab, Year)

shdata_CRvTR_clean$Year = factor(shdata_CRvTR_clean$Year)
levels(shdata_CRvTR_clean$Year) = c("2009", "2010", "2012", "2016")


p2 <- 
  ggplot(shdata_CRvTR_clean, aes(x=Time, y=SL, color=Year))+
  #geom_jitter(aes(shape=CaptureType), alpha = 0.6)+
  geom_line(data=df_fake, 
            aes(x=fake_x, y=fake_y, color=Year, linetype=factor(fake_hab)))+
  theme_classic()+
  labs(x="", y="") +
  facet_wrap(~Year, ncol = 3, scales = "free") +
  scale_shape_manual("Type", labels=c("Recaptured\n(Treated)", "Recaptured\n(Control)"), values=c(20, 0))+
  scale_linetype_manual("Type", labels=c("Recaptured\n(Treated)", "Recaptured\n(Control)"), values=c(1,2)) +
  scale_color_manual("Year", values=c("#E41A1C", "#377EB8", "#4DAF4A", "#FF7F00"), 
                     guide = "none") +
  scale_y_continuous(limits = c(30, 80), breaks=c(35,55,75))+
  theme(axis.text = element_text(size = 12, color = "black"),
        axis.title = element_text(size = 13, color = "black"),
        strip.background = element_blank(),
        strip.text = element_text(size = 12, color = "black"),
        legend.justification = c("right", "top"),
        legend.position = c(0.9, 0.35),
        legend.text = element_text(size = 12, color = "black"),
        legend.key.width = unit(1, "cm"),
        legend.key.height = unit(1.2, "cm")) +
  guides(shape = guide_legend(override.aes = list(size = 0.5, stroke = 1.75))) 

#quartz(height=5, width=7)
quartz(height=5.5, width = 10)
p2
quartz.save("./figures/Steelhead/CRvTR/Steelhead_data_predictions.pdf", type="pdf", dpi=300)



### Average among years:

fake_y_avg <- NULL

for(i in 1:N_habitat){
  temp <- (median(all_posts$Yp_hab[,i])) +
    ((median(all_posts$Yinf_hab[,i])) /
       ( 1 + exp( (median(all_posts$alpha_hat_hab[,i]))  -
                    (median(all_posts$a_hab[,i])) * fake_x
       )))
  
  fake_y_avg <- c(fake_y_avg, temp)
}

fake_hab_avg <- rep(c(1:N_habitat), each=length(fake_x))
df_fake_avg <- data.frame(fake_x, fake_y_avg, fake_hab_avg)

p3 =
  ggplot(shdata_CRvTR_clean, aes(x=Time, y=SL))+
  geom_jitter(aes(shape=factor(Habitat), color = Year), alpha = 0.5)+
  geom_line(data=df_fake_avg, 
            aes(x=fake_x, y=fake_y_avg, linetype=factor(fake_hab_avg)),
            size = 1)+
  theme_classic()+
  labs(x="", y="") +
  scale_shape_manual("Type", labels=c("Recaptured\n(Treated)", "Recaptured\n(Control)"), values=c(20, 0))+
  scale_linetype_manual("Type", labels=c("Recaptured\n(Treated)", "Recaptured\n(Control)"), values=c(1,2)) +
  scale_color_manual("Year", values=c("#E41A1C", "#377EB8", "#4DAF4A", "#FF7F00"), 
                     guide = "none") +
  scale_y_continuous(limits = c(15, 100), breaks=c(25,50,75,100))+
  theme(axis.text = element_text(size = 12, color = "black"),
        axis.title = element_text(size = 13, color = "black"),
        strip.background = element_blank(),
        strip.text = element_text(size = 12, color = "black"),
        legend.justification = c("right", "top"),
        legend.position = c(0.975, 0.378),
        legend.text = element_text(size = 12, color = "black"),
        legend.key.width = unit(0.8, "cm")) +
  guides(shape = FALSE, linetype = FALSE, color = guide_legend(override.aes = list(shape = 15, alpha = 1))) 

quartz(height=5, width = 7)
p3


## Combine p2 and p3
plot_sh_combo =
  arrangeGrob(p3, p2,
              ncol = 4,
              layout_matrix = matrix(c(1,1,2,2), ncol = 4),
              widths = c(0.7,0.7,1,1),
              left = textGrob("Standard Length (mm)", rot = 90,
                              gp=gpar(fontsize = 12, fontface = "bold")),
              bottom = textGrob("Time Point (days)", 
                                gp=gpar(fontsize = 12, fontface = "bold")))
quartz(height=5, width = 12, dpi=300)
grid.arrange(plot_sh_combo)



# p3 <- p2
# p3$layers <- p3$layers[-1]
# p3 <- p3 +
#   scale_linetype_manual("Type", labels=c("Recaptured\n(Treated)", "Recaptured\n(Control)"), values=c(1,4))
# 
# quartz(height=5, width=7)
# p3
# quartz.save("./figures/Steelhead/CRvTR/Steelhead_just_predictions.pdf", type="pdf", dpi=300)

