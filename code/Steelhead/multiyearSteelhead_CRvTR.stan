//Multiyear Steelhead model stan file 
//6/24/2016 - Joe Mihaljevic, Ruby An 

data {
  int< lower = 1 > N_habitat;
  int< lower = 1 > N_year;
  int< lower = 1 > N_recap_T;
  int< lower = 1 > N_recap_C;
  int< lower = 1 > Nobs_T;
  int< lower = 1 > Nobs_C;
  int< lower = 1 > Nobs_total;
  
  int< lower = 1, upper = N_habitat> Habitat[Nobs_total];
  int< lower = 1, upper = N_year> Year[Nobs_total];
  int< lower = 1> FishID[Nobs_total];
  // int< lower = 0, upper = N_recap_T> FishID_T[Nobs_T];
  // int< lower = 0, upper = N_recap_C> FishID_C[Nobs_C];

  
  vector< lower = 0 >[Nobs_total] SL; // vector of real numbers w/lower bound 0
  vector< lower = 0 >[Nobs_total] Date;
  
}

parameters {
  //habitat covariate: fixed effect parameters
  vector< lower = 25 >[N_habitat] Yp_hab; // vector of real numbers w/lower bound 0
  vector< lower = 0 >[N_habitat] Yinf_hab;
  vector< lower = 0, upper = 10 >[N_habitat] alpha_hat_hab;
  vector< lower = 0, upper = 1 >[N_habitat] a_hab;
  
  //year covariate: random effect etas
  vector[N_year] eta_Yp[N_habitat];
  vector[N_year] eta_Yinf[N_habitat];
  vector[N_year] eta_alpha_hat[N_habitat];
  vector[N_year] eta_a[N_habitat];
  
  //random effect of fish identification (for recaps)
  // real eta_fish_Yp[N_recap];
  // real eta_fish_Yinf[N_recap];
  // real eta_fish_alpha_hat[N_recap];
  // real eta_fish_a[N_recap];
  real eta_fish_T[N_recap_T];
  real eta_fish_C[N_recap_C];
  
  //std dev for random effs
  real<lower = 0> sigma_Yp;
  real<lower = 0> sigma_Yinf;
  real<lower = 0> sigma_alpha_hat; 
  real<lower = 0> sigma_a;
  // real<lower = 0> sigma_fish_Yp;
  // real<lower = 0> sigma_fish_Yinf;
  // real<lower = 0> sigma_fish_alpha_hat;
  // real<lower = 0> sigma_fish_a;
  real<lower = 0> sigma_fish_T;
  real<lower = 0> sigma_fish_C;
  
  //unexplained variation
  real<lower = 0> sigma_resid[N_habitat];
  
}

transformed parameters  {
  vector[Nobs_total] Yp;
  vector[Nobs_total] Yinf;
  vector[Nobs_total] alpha_hat;
  vector[Nobs_total] a;
  vector[Nobs_total] mu;
  
  
  for(i in 1:Nobs_total){
    
    Yp[i] = Yp_hab[Habitat[i]] + eta_Yp[Habitat[i], Year[i]];
    Yinf[i] = Yinf_hab[Habitat[i]] + eta_Yinf[Habitat[i], Year[i]];
    alpha_hat[i] = alpha_hat_hab[Habitat[i]] + eta_alpha_hat[Habitat[i], Year[i]];
    a[i] = a_hab[Habitat[i]] + eta_a[Habitat[i], Year[i]];
    
    // if(FishID[i] != 0){ //these are the recaptured individuals
    //   Yp[i] = Yp_hab[Habitat[i]] + eta_Yp[Habitat[i], Year[i]] + eta_fish_Yp[FishID[i]];
    //   Yinf[i] = Yinf_hab[Habitat[i]] + eta_Yinf[Habitat[i], Year[i]] + eta_fish_Yinf[FishID[i]];
    //   alpha_hat[i] = alpha_hat_hab[Habitat[i]] + eta_alpha_hat[Habitat[i], Year[i]] + eta_fish_alpha_hat[FishID[i]];
    //   a[i] = a_hab[Habitat[i]] + eta_a[Habitat[i], Year[i]] + eta_fish_a[FishID[i]];
    // }else{
    //   Yp[i] = Yp_hab[Habitat[i]] + eta_Yp[Habitat[i], Year[i]];
    //   Yinf[i] = Yinf_hab[Habitat[i]] + eta_Yinf[Habitat[i], Year[i]];
    //   alpha_hat[i] = alpha_hat_hab[Habitat[i]] + eta_alpha_hat[Habitat[i], Year[i]];
    //   a[i] = a_hab[Habitat[i]] + eta_a[Habitat[i], Year[i]];
    // }
    
  }

  
  for (i in 1:Nobs_total) { // may be able to vectorize... 
    if(Habitat[i] == 1){ // treated (comes first in the data set)
      mu[i] = Yp[i] + (Yinf[i]/(1+exp(alpha_hat[i] - a[i]*Date[i]))) + eta_fish_T[FishID[i]];
    }else{ // control
      mu[i] = Yp[i] + (Yinf[i]/(1+exp(alpha_hat[i] - a[i]*Date[i]))) + eta_fish_C[FishID[i]];
    }
  }
  
}

model {
  // Priors
  sigma_Yp ~ normal(0,1); // might need to play with 2
  sigma_Yinf ~ normal(0,1);
  sigma_alpha_hat ~ normal(0,1);
  sigma_a ~ normal(0,1);
  // sigma_fish_Yp ~ normal(0,2);
  // sigma_fish_Yinf ~ normal(0,2);
  // sigma_fish_alpha_hat ~ normal(0,2);
  // sigma_fish_a ~ normal(0,2);
  sigma_fish_T ~ normal(0,5);
  sigma_fish_C ~ normal(0,5);
  sigma_resid ~ normal(0,15);
  
  for(i in 1:N_habitat){
    eta_Yp[i] ~ normal(0, sigma_Yp); // this is vectorized to N_year, implied 
    eta_Yinf[i] ~ normal(0, sigma_Yinf);
    eta_alpha_hat[i] ~ normal(0, sigma_alpha_hat);
    eta_a[i] ~ normal(0, sigma_a); 
  }
  
  // eta_fish_Yp ~ normal(0, sigma_fish_Yp); //vectorized
  // eta_fish_Yinf ~ normal(0, sigma_fish_Yinf); //vectorized
  // eta_fish_alpha_hat ~ normal(0, sigma_fish_alpha_hat); //vectorized
  // eta_fish_a ~ normal(0, sigma_fish_a); //vectorized

  eta_fish_T ~ normal(0, sigma_fish_T);
  eta_fish_C ~ normal(0, sigma_fish_C);
  
  Yp_hab ~ normal(40, 5); 
  Yinf_hab ~ normal(30, 5);
  alpha_hat_hab ~ uniform(0, 1000);
  
  a_hab ~ uniform(0, 1);
  //a_hab ~ normal(0.5, 0.5);
  
  // for(i in 1:Nobs_total){
  //   SL[i] ~ normal(mu[i], sigma_resid[Habitat[i]]);
  // }
  
  SL ~ normal(mu, sigma_resid[Habitat]);
  //SL ~ normal(mu, sigma_resid);
}

generated quantities {
  real diff_Yp;
  real diff_Yinf;
  real diff_alpha_hat;
  real diff_a;
  
  real diff_Yp_y[N_year];
  real diff_Yinf_y[N_year];
  real diff_alpha_hat_y[N_year];
  real diff_a_y[N_year];
  
  real total_growth[N_habitat];
  
  diff_Yp = Yp_hab[1] - Yp_hab[2];
  diff_Yinf = Yinf_hab[1] - Yinf_hab[2];
  diff_alpha_hat = alpha_hat_hab[1] - alpha_hat_hab[2];
  diff_a = a_hab[1] - a_hab[2];
  
  for(i in 1:N_year){
    diff_Yp_y[i] = (Yp_hab[1] + eta_Yp[1,i]) - (Yp_hab[2] + eta_Yp[2,i]);
    diff_Yinf_y[i] = (Yinf_hab[1] + eta_Yinf[1,i]) - (Yinf_hab[2] + eta_Yinf[2,i]);
    diff_alpha_hat_y[i] = (alpha_hat_hab[1] + eta_alpha_hat[1,i]) - (alpha_hat_hab[2] + eta_alpha_hat[2,i]);
    diff_a_y[i] = (a_hab[1] + eta_a[1,i]) - (a_hab[2] + eta_a[2,i]);
  }
  
  for(i in 1:N_habitat){
    total_growth[i] = Yp_hab[i] + Yinf_hab[i];
  }
  
}
