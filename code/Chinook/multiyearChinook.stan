//Multiyear Chinook Salmon model stan file 

data {
  
  // Sample sizes:
  int< lower = 1 > N_habitat;
  int< lower = 1 > N_year;
  int< lower = 1 > N_recap;
  int< lower = 1 > Nobs_total;
  
  // Identity vectors:
  int< lower = 1, upper = N_habitat> Habitat[Nobs_total];
  int< lower = 1, upper = N_year> Year[Nobs_total];
  int< lower = 0, upper = N_recap> FishID[Nobs_total];
  
  // Observations:
  vector< lower = 0 >[Nobs_total] SL; //Length
  vector< lower = 0 >[Nobs_total] Date; //Time
  
}

parameters {
  
  //habitat-specific fixed effect coefficients
  vector< lower = 40 >[N_habitat] Yp_hab; 
  vector< lower = 0 >[N_habitat] Yinf_hab;
  vector< lower = 0, upper = 10 >[N_habitat] alpha_hat_hab;
  vector< lower = 0, upper = 1 >[N_habitat] a_hab;
  
  //random effects of year, habitat-specific
  vector[N_year] eta_Yp[N_habitat];
  vector[N_year] eta_Yinf[N_habitat];
  vector[N_year] eta_alpha_hat[N_habitat];
  vector[N_year] eta_a[N_habitat];
  
  //random effect of fish identity (for recaptured fish only)
  real eta_fish[N_recap];
  
  //std dev for random effs
  real<lower = 0> sigma_Yp;
  real<lower = 0> sigma_Yinf;
  real<lower = 0> sigma_alpha_hat; 
  real<lower = 0> sigma_a;
  real<lower = 0> sigma_fish;
  
  //unexplained (residual) variation: habitat-specific
  real<lower = 0> sigma_resid[N_habitat];
  
}

transformed parameters  {
  
  // This block is used to calculate the observation-level expected values,
  // based on the functional growth model. This is more computationally efficient 
  // than calculating these values in the "model" block. 
  
  vector[Nobs_total] Yp;
  vector[Nobs_total] Yinf;
  vector[Nobs_total] alpha_hat;
  vector[Nobs_total] a;
  vector[Nobs_total] mu;
  
  
  for(i in 1:Nobs_total){
    
    // For a given observation, the overall parameter is
    // the sum of the fixed, habitat-specific effect and the 
    // habitat- and year-specific random effects. 
    
    Yp[i] = Yp_hab[Habitat[i]] + eta_Yp[Habitat[i], Year[i]];
    Yinf[i] = Yinf_hab[Habitat[i]] + eta_Yinf[Habitat[i], Year[i]];
    alpha_hat[i] = alpha_hat_hab[Habitat[i]] + eta_alpha_hat[Habitat[i], Year[i]];
    a[i] = a_hab[Habitat[i]] + eta_a[Habitat[i], Year[i]];
    
  }


  for (i in 1:Nobs_total) { 
    
    // The expected values (mu's), based on the functional growth curve.
    // Expected values incorporate the random effect of fish ID (for recaptured fish only).
    
    if(FishID[i] > 0){ // If it was recaptured
      
      mu[i] = Yp[i] + (Yinf[i]/(1+exp(alpha_hat[i] - a[i]*Date[i]))) + eta_fish[FishID[i]];
    
    }else{ // Control fish (FishID == 0)
    
      mu[i] = Yp[i] + (Yinf[i]/(1+exp(alpha_hat[i] - a[i]*Date[i])));
    
    }
    
  }
  
}

model {
  
  ////////////
  // Priors //
  ////////////
  
  // Half-normal priors for random effects (std dev)
  sigma_Yp ~ normal(0,1); 
  sigma_Yinf ~ normal(0,1);
  sigma_alpha_hat ~ normal(0,1);
  sigma_a ~ normal(0,1);
  sigma_fish ~ normal(0,5);
  sigma_resid ~ normal(0,15); // vectorized over N_habitat
  
  for(i in 1:N_habitat){
    
    // Distributions of random effects.
    // Note, these are vectorized over N_year (e.g. dimensions of eta_Yp[N_habitat, N_year])
    
    eta_Yp[i] ~ normal(0, sigma_Yp); 
    eta_Yinf[i] ~ normal(0, sigma_Yinf);
    eta_alpha_hat[i] ~ normal(0, sigma_alpha_hat);
    eta_a[i] ~ normal(0, sigma_a); 
  
  }
  
  // FishID random effect (vectorized)
  eta_fish ~ normal(0, sigma_fish);
  
  // Fixed, habitat-specific effects (vectorized)
  Yp_hab ~ normal(55, 5); 
  Yinf_hab ~ normal(23, 5);
  alpha_hat_hab ~ uniform(0, 1000);
  a_hab ~ uniform(0, 1);

  ////////////////
  // LIKELIHOOD //
  ////////////////
  
  // (vectorized over all observations)
  SL ~ normal(mu, sigma_resid[Habitat]);
  
}

generated quantities {
  
  ////////////////////////////////////////////////////////
  // POSTERIOR INFERENCE FOR HABITAT- AND YEAR-SPECIFIC //
  // DIFFERENCES BETWEEN PARAMETER ESTIMATES            //
  ////////////////////////////////////////////////////////
  
  // AMONG YEAR DIFFERENCES
  real diff_Yp;
  real diff_Yinf;
  real diff_alpha_hat;
  real diff_a;
  
  // YEAR-SPECIFIC DIFFERENCES
  real diff_Yp_y[N_year];
  real diff_Yinf_y[N_year];
  real diff_alpha_hat_y[N_year];
  real diff_a_y[N_year];
  
  // TOTAL GROWTH (Yp + Yinf)
  real total_growth[N_habitat];
  
  
  diff_Yp = Yp_hab[1] - Yp_hab[2];
  diff_Yinf = Yinf_hab[1] - Yinf_hab[2];
  diff_alpha_hat = alpha_hat_hab[1] - alpha_hat_hab[2];
  diff_a = a_hab[1] - a_hab[2];
  
  for(i in 1:N_year){
    
    // These year-specific differences account for yearly random effects:  
    
    diff_Yp_y[i] = (Yp_hab[1] + eta_Yp[1,i]) - (Yp_hab[2] + eta_Yp[2,i]);
    diff_Yinf_y[i] = (Yinf_hab[1] + eta_Yinf[1,i]) - (Yinf_hab[2] + eta_Yinf[2,i]);
    diff_alpha_hat_y[i] = (alpha_hat_hab[1] + eta_alpha_hat[1,i]) - (alpha_hat_hab[2] + eta_alpha_hat[2,i]);
    diff_a_y[i] = (a_hab[1] + eta_a[1,i]) - (a_hab[2] + eta_a[2,i]);
    
  }
  
  for(i in 1:N_habitat){
    
    total_growth[i] = Yp_hab[i] + Yinf_hab[i];
  
  }
  
}
