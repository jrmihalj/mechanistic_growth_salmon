# Plotting the differences between parameters
# chinook and steelhead
# Author: J. Mihaljevic

##################
##################

library(tidyverse)
library(grid)
library(gridExtra)

##################
##################

# Plotting theme:

theme_joe = function(){
  theme_classic(base_size = 12) +
    theme(axis.title = element_text(color = "black"),
          axis.text = element_text(color = "black"),
          axis.title.y = element_text(size = 14))
} 

##################
##################

# CHINOOK

chin_df = read.csv(file="./code/Param_Diffs/chin_params.csv")
chin_df =
  chin_df %>%
  mutate(Plus = High_Dif - Med_Dif,
         Minus = Med_Dif - Low_Dif) %>%
  mutate(Median = -1*Med_Dif) %>%
  mutate(Low_95 = Median - Minus,
         High_95 = Median + Plus)
  
p1 =
  chin_df %>% 
  filter(Param == "alpha_hat") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  #scale_y_continuous(limits = c(-50, 50)) +
  labs(x = "", y = expression(Delta~hat(alpha))) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()

p2 =
  chin_df %>% 
  filter(Param == "a") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~a)) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()


p3 =
  chin_df %>% 
  filter(Param == "Intercept") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~"Length at time = 0 (Intercept)")) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()

# p3 =
#   chin_df %>% 
#   filter(Param == "y.prime") %>%
#   ggplot(aes(x = Year, y = Median)) +
#   geom_point(shape = 19, size = 1) +
#   geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
#   labs(x = "", y = expression(Delta~Y*minute)) +
#   geom_hline(yintercept = 0, linetype = 2) +
#   theme_joe()

p4 =
  chin_df %>% 
  filter(Param == "y.inf") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~Y[infinity])) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()

plot_combo_chin =
  arrangeGrob(p1,p2,p3,p4, 
              ncol = 2, nrow = 2,
              bottom = textGrob("Year"))

quartz(height = 6, width=8)
grid.arrange(plot_combo_chin)


##################
##################

# Steelhead

steel_df = read.csv(file="./code/Param_Diffs/steel_params.csv")
steel_df =
  steel_df %>%
  mutate(Plus = High_Dif - Med_Dif,
         Minus = Med_Dif - Low_Dif) %>%
  mutate(Median = Med_Dif) %>%
  mutate(Low_95 = Median - Minus,
         High_95 = Median + Plus)


p5 =
  steel_df %>% 
  filter(Param == "alpha_hat") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~hat(alpha))) +
  #scale_y_continuous(limits = c(-100, 50)) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()

p6 =
  steel_df %>% 
  filter(Param == "a") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~a)) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()

p7 =
  steel_df %>% 
  filter(Param == "Intercept") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~"Length at time = 0 (Intercept)")) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()


# p7 =
#   steel_df %>% 
#   filter(Param == "y.p") %>%
#   ggplot(aes(x = Year, y = Median)) +
#   geom_point(shape = 19, size = 1) +
#   geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
#   labs(x = "", y = expression(Delta~Y*minute)) +
#   geom_hline(yintercept = 0, linetype = 2) +
#   theme_joe()

p8 =
  steel_df %>% 
  filter(Param == "y.inf") %>%
  ggplot(aes(x = Year, y = Median)) +
  geom_point(shape = 19, size = 1) +
  geom_errorbar(aes(ymin = Low_95, ymax = High_95), width = 0.4) +
  labs(x = "", y = expression(Delta~Y[infinity])) +
  geom_hline(yintercept = 0, linetype = 2) +
  theme_joe()

plot_combo_steel =
  arrangeGrob(p5,p6,p7,p8, 
              ncol = 2, nrow = 2,
              bottom = textGrob("Year"))

quartz(height = 6, width=8)
grid.arrange(plot_combo_steel)
